Тестовое задание WebAPI выполнено с использованием Spring и postgresql


База данных web_api_db

Пользователь web_api с паролем wep_api


Для запуска:

1) создать БД

sudo -u postgres psql 

CREATE DATABASE web_api_db;

CREATE USER web_api WITH password 'web_api';

GRANT ALL ON DATABASE web_api_db TO web_api;


2) Запустить проект

cd <папка с проектом>

mvn spring-boot:run


Добавления записи в бд:

POST http://localhost:8080/api/Sample

Выборка записи:

GET http://localhost:8080/api/Sample?id=670ae5d3-c33f-4773-bad7-6dce22354d9c