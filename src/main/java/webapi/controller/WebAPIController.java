package webapi.controller;

import webapi.database.entity.WebAPIEntity;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import webapi.database.service.WebAPIService;
import webapi.utils.JsonParser;

import java.time.format.DateTimeParseException;

@RestController
@RequestMapping(value = "/api")
public class WebAPIController {

    @Autowired
    JsonParser jsonParser;

    @Autowired
    WebAPIService webAPIService;

    @RequestMapping(value = "/Sample", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<String> getSample(@RequestParam("id") String id) {
        WebAPIEntity entity = webAPIService.findOne(id);
        if (entity == null) {
            return new ResponseEntity<>(String.format("Id %s did not found!", id), null, HttpStatus.NOT_FOUND);
        }

        try {
            String json = jsonParser.toJson(entity);
            return new ResponseEntity<>(json, null, HttpStatus.OK);
        } catch (ParseException e) {
            return new ResponseEntity<>("Something went wrong!", null, HttpStatus.NO_CONTENT);
        }
    }

    @RequestMapping(value = "/Sample", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> postSample(@RequestBody String body) {
        try {
            WebAPIEntity entity = jsonParser.fromJson(body);
            webAPIService.save(entity);
            return new ResponseEntity<>(entity.getId(), null, HttpStatus.OK);
        } catch (DateTimeParseException | ParseException e) {
            return new ResponseEntity<>("Can not parse Json!", null, HttpStatus.BAD_REQUEST);
        }
    }
}
