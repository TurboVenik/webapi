package webapi.database.entity;

import org.hibernate.annotations.GenericGenerator;
import webapi.utils.ZonedDateTimeAttributeConverter;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Entity
public class WebAPIEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    private String id;

    @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE")
    @Convert(converter = ZonedDateTimeAttributeConverter.class)
    private ZonedDateTime date;
    private String data;

    public String getId() {
        return id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public ZonedDateTime getDate() {
        return date;
    }

    public void setDate(ZonedDateTime date) {
        this.date = date;
    }
}
