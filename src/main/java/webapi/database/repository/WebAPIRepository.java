package webapi.database.repository;

import webapi.database.entity.WebAPIEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WebAPIRepository extends CrudRepository<WebAPIEntity, String> {

    public List<WebAPIEntity> findByData(String data);
}