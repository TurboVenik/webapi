package webapi.database.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import webapi.database.entity.WebAPIEntity;
import webapi.database.repository.WebAPIRepository;

import java.util.List;

@Component
public class WebAPIService {

    @Autowired
    private WebAPIRepository webAPIRepository;


    public List<WebAPIEntity> findByData(String data) {
        return webAPIRepository.findByData(data);
    }

    public WebAPIEntity findOne(String id) {
        return webAPIRepository.findOne(id);
    }

    public WebAPIEntity save(WebAPIEntity entity) {
        return webAPIRepository.save(entity);
    }
}
