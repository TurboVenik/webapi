package webapi.utils;

import webapi.database.entity.WebAPIEntity;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

@Component
public class JsonParser {

    private JSONParser parser = new JSONParser();

    public String toJson(WebAPIEntity entity) throws ParseException {
        JSONObject json = new JSONObject();

        json.put("Data", parser.parse(entity.getData()));
        json.put("Date", entity.getDate().toString());

        return json.toString();
    }

    public WebAPIEntity fromJson(String json) throws ParseException, DateTimeParseException {
        JSONObject o = (JSONObject)parser.parse(json);

        if (o.size() != 2 || !(o.get("Data") instanceof JSONArray)) {
            throw new ParseException(ParseException.ERROR_UNEXPECTED_EXCEPTION);
        }
        String date = o.get("Date").toString();
        ZonedDateTime zonedDateTime = ZonedDateTime.parse(date, DateTimeFormatter.ISO_DATE_TIME);

        String data = (o.get("Data")).toString();

        WebAPIEntity entity = new WebAPIEntity();
        entity.setData(data);
        entity.setDate(zonedDateTime);
        return entity;
    }
}
