package webapi;

import org.json.simple.parser.ParseException;
import org.junit.Test;
import webapi.database.entity.WebAPIEntity;
import webapi.utils.JsonParser;
import java.time.ZonedDateTime;

import static org.assertj.core.api.Assertions.assertThat;

public class JsonParserTest {

    private JsonParser jsonParser = new JsonParser();

    private String data = "[{\"Value\":123123,\"Name\":\"Sample 1\"},{\"Value\":123,\"Name\":\"Sample 2\"}]";

    private String json = "{\"Data\":[{\"Value\":123123,\"Name\":\"Sample 1\"},{\"Value\":123,\"Name\":\"Sample 2\"}],\"Date\":\"2018-05-04T03:28:24.191Z[Africa\\/Nouakchott]\"}";

    @Test
    public void toJson() throws ParseException {
        ZonedDateTime time = ZonedDateTime.now();

        WebAPIEntity entity = new WebAPIEntity();
        entity.setData(data);
        entity.setDate(time);

        String json = jsonParser.toJson(entity);

        String stringTime = time.toString().replace("/", "\\/");
        String expected = String.format("{\"Data\":%s,\"Date\":\"%s\"}", data, stringTime);

        assertThat(json)
                .isEqualTo(expected);
    }

    @Test
    public void fromJson() throws ParseException {
        WebAPIEntity entity = jsonParser.fromJson(json);

        assertThat(entity.getDate())
                .isEqualTo(ZonedDateTime.parse("2018-05-04T03:28:24.191Z[Africa/Nouakchott]"));
        assertThat(entity.getData())
                .isEqualTo(data);
    }
}
