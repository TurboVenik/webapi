package webapi;

import org.json.simple.parser.ParseException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import webapi.controller.WebAPIController;
import webapi.database.entity.WebAPIEntity;
import webapi.database.service.WebAPIService;
import webapi.utils.JsonParser;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@WebMvcTest(WebAPIController.class)
public class WebAPIControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private WebAPIService webAPIService;

    @MockBean
    private JsonParser jsonParser;

    @Before
    public void setUp() throws ParseException {
        WebAPIEntity entity = new WebAPIEntity();
        Mockito.when(webAPIService.findOne("id")).thenReturn(entity);
        Mockito.when(jsonParser.toJson(entity)).thenReturn("json");
        Mockito.when(jsonParser.fromJson("json")).thenReturn(entity);
    }

    @Test
    public void getSample() throws Exception {
        mockMvc.perform(get("/api/Sample")
                    .contentType(MediaType.APPLICATION_JSON)
                    .param("id", "id"))
                .andExpect(status().isOk());
    }

    @Test
    public void postSample() throws Exception {
        mockMvc.perform(post("/api/Sample")
                .contentType(MediaType.APPLICATION_JSON)
                .content("json"))
                .andExpect(status().isOk());
    }

    @Configuration
    @ComponentScan({"webapi/controller", "webapi/database/service", "webapi/utils"})
    public static class TestConf {}
}
