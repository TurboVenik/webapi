package webapi;

import webapi.database.entity.WebAPIEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import webapi.database.repository.WebAPIRepository;

import java.time.ZonedDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class WebAPIRepositoryTest {

    @Autowired
    private WebAPIRepository repository;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void findOne() {
        WebAPIEntity entity = new WebAPIEntity();
        entity.setData("data");
        entity.setDate(ZonedDateTime.now());
        entityManager.persist(entity);
        entityManager.flush();
        WebAPIEntity one = repository.findOne(entity.getId());
        assertThat(entity)
                .isEqualTo(one);
    }

    @Test
    public void findByData() {
        WebAPIEntity entity = new WebAPIEntity();
        entity.setData("data");
        entity.setDate(ZonedDateTime.now());
        entityManager.persist(entity);
        entityManager.flush();

        List<WebAPIEntity> findByData = repository.findByData("data");

        assertThat(findByData.size()).isEqualTo(1);
        assertThat(entity)
                .isEqualTo(findByData.get(0));
    }
}
