package webapi;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import webapi.database.entity.WebAPIEntity;
import webapi.database.repository.WebAPIRepository;
import webapi.database.service.WebAPIService;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;

@RunWith(SpringRunner.class)
public class WebAPIServiceTest {

    @TestConfiguration
    static class EmployeeServiceImplTestContextConfiguration {

        @Bean
        public WebAPIService employeeService() {
            return new WebAPIService();
        }
    }

    @Autowired
    private WebAPIService webAPIService;

    @MockBean
    private WebAPIRepository webAPIRepository;

    @Before
    public void setUp() {
        WebAPIEntity entity = new WebAPIEntity();
        entity.setData("data");

        List<WebAPIEntity> list = Arrays.asList(entity);

        Mockito.when(webAPIRepository.findByData(entity.getData())).thenReturn(list);
    }

    @Test
    public void findByName() {
        List<WebAPIEntity> data = webAPIService.findByData("data");

        assertThat(data.size())
                .isEqualTo(1);
        assertThat(data.get(0).getData())
                .isEqualTo("data");
    }
}
